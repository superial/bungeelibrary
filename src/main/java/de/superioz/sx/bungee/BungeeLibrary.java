package de.superioz.sx.bungee;

import de.superioz.sx.bungee.command.CommandErrorEvent;
import de.superioz.sx.bungee.command.CommandHandler;
import de.superioz.sx.bungee.command.verification.MethodVerifierCommand;
import de.superioz.sx.bungee.exception.CommandRegisterException;
import de.superioz.sx.java.util.Consumer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Event;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created on 07.03.2016.
 */
public class BungeeLibrary {

	private static Plugin plugin;

	/**
	 * Initialises the sx with given bungeecord plugin instance
	 *
	 * @param bungeePlugin The plugin instance
	 */
	public static void initFor(Plugin bungeePlugin){
		plugin = bungeePlugin;

		// Register command
		try{
			registerCommand(new Consumer<CommandErrorEvent>() {
				@Override
				public void accept(CommandErrorEvent event){
					//Nothing
				}
			}, MethodVerifierCommand.class);
		}
		catch(CommandRegisterException e){
			e.printStackTrace();
		}
	}

	/**
	 * Registers given command classes
	 *
	 * @param commandClass      The main class
	 * @param subCommandClasses The sub commands
	 */
	public static void registerCommand(Consumer<CommandErrorEvent> onError, Class<?> commandClass, Class<?>... subCommandClasses) throws CommandRegisterException{
		CommandHandler.registerCommand(onError, commandClass, subCommandClasses);
	}

	// -- Intern methods

	public static Plugin plugin(){
		return plugin;
	}

	public static ProxyServer proxy(){
		return plugin().getProxy();
	}

	public static void callEvent(Event event){
		proxy().getPluginManager().callEvent(event);
	}

}
