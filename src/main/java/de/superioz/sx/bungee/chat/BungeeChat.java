package de.superioz.sx.bungee.chat;

import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.util.ChatUtil;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Collection;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public class BungeeChat {

	/**
	 * Broadcasts a message
	 * @param message The message
	 */
	public static void broadcast(BaseComponent[] message){
		Collection<ProxiedPlayer> players = BungeeLibrary.proxy().getPlayers();
		send(message, players.toArray(new ProxiedPlayer[players.size()]));
	}

	public static void broadcast(WrappedMessage message){
		Collection<ProxiedPlayer> players = BungeeLibrary.proxy().getPlayers();
		send(message, players.toArray(new ProxiedPlayer[players.size()]));
	}

	public static void broadcast(String message){
		Collection<ProxiedPlayer> players = BungeeLibrary.proxy().getPlayers();
		send(message, players.toArray(new ProxiedPlayer[players.size()]));
	}

	public static void broadcast(String message, String prefix){
		Collection<ProxiedPlayer> players = BungeeLibrary.proxy().getPlayers();
		send(message, prefix, players.toArray(new ProxiedPlayer[players.size()]));
	}

	/**
	 * Sends a message to given players with prefix (message)
	 *
	 * @param message The message
	 * @param prefix  The prefix
	 * @param players The players
	 */
	public static void send(String message, String prefix, ProxiedPlayer... players){
		send(prefix + " " + message, players);
	}

	public static void send(String message, String prefix, CommandSender... senders){
		send(prefix + " " + message, senders);
	}

	public static void send(WrappedMessage message, String prefix, ProxiedPlayer... players){
		send(message.setPrefix(prefix, null, null), players);
	}

	public static void send(WrappedMessage message, String prefix, CommandSender... senders){
		send(message.setPrefix(prefix, null, null), senders);
	}

	/**
	 * Sends a message to given players (baseComponent)
	 *
	 * @param message The message
	 * @param players The players
	 */
	public static void send(BaseComponent[] message, ChatMessageType type, ProxiedPlayer... players){
		for(ProxiedPlayer p : players){
			p.sendMessage(type, message);
		}
	}

	public static void send(BaseComponent[] message, CommandSender... senders){
		for(CommandSender s : senders){
			s.sendMessage(message);
		}
	}

	/**
	 * Sends a message to given players (wrappedMessage)
	 *
	 * @param message The message
	 * @param players The players
	 */
	public static void send(WrappedMessage message, ChatMessageType type, ProxiedPlayer... players){
		for(ProxiedPlayer p : players){
			p.sendMessage(type, message);
		}
	}

	public static void send(WrappedMessage message, ProxiedPlayer... players){
		for(ProxiedPlayer p : players){
			p.sendMessage(message);
		}
	}

	public static void send(WrappedMessage message, CommandSender... senders){
		for(CommandSender s : senders){
			s.sendMessage(message);
		}
	}

	/**
	 * Sends a message to given players (string)
	 *
	 * @param message The message
	 * @param players The players
	 */
	public static void send(String message, ChatMessageType type, ProxiedPlayer... players){
		send(TextComponent.fromLegacyText(ChatUtil.fabulize(message)), type, players);
	}

	public static void send(BaseComponent[] message, ProxiedPlayer... players){
		send(message, ChatMessageType.CHAT, players);
	}

	public static void send(String message, CommandSender... senders){
		send(TextComponent.fromLegacyText(ChatUtil.fabulize(message)), senders);
	}

	public static void send(String message, ProxiedPlayer... players){
		send(message, ChatMessageType.CHAT, players);
	}

}
