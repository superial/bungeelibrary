package de.superioz.sx.bungee.command;

import de.superioz.sx.java.util.ListUtil;
import lombok.Getter;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
@Getter
public class CommandWrapper {

	private String label, description, permission, usage, parentCommand;
	private List<String> aliases;
	private int min, max;
	private AllowedCommandSender allowedSender;
	private List<Class<?>> subCommandClasses = new ArrayList<>();
	private List<CommandWrapper> subCommands = new ArrayList<>();
	private CommandType commandType;
	private CommandWrapper parent;
	private List<String> flags;

	protected Class parentClass;
	protected Method parentMethod;

	public CommandWrapper(Method method, CommandType type){
		this.parentMethod = method;
		this.parentClass = method.getDeclaringClass();
		this.commandType = type;

		if(getCommandType() == CommandType.NESTED){
			Command.Nested annotation = this.parentMethod.getAnnotation(Command.Nested.class);
			this.parentCommand = annotation.parent();
		}

		Command annotation = this.parentMethod.getAnnotation(Command.class);
		if(getCommandType() == CommandType.ROOT){
			annotation = (Command)getParentClass().getAnnotation(Command.class);
		}
		this.label = annotation.label();
		this.description = annotation.desc();
		this.usage = annotation.usage();
		this.aliases = Arrays.asList(annotation.aliases());
		this.min = annotation.min();
		this.max = annotation.max();
		this.allowedSender = annotation.commandTarget();
		this.permission = annotation.permission();
		this.flags = Arrays.asList(annotation.flags());
	}

	/**
	 * Get subcommand with given label
	 *
	 * @param label The label
	 * @return The command
	 */
	public CommandWrapper getSubCommand(String label){
		if(getSubCommands() == null)
			return null;

		for(CommandWrapper cw : getSubCommands()){
			if(cw.getLabel().equalsIgnoreCase(label)
					|| ListUtil.listContains(cw.getAliases().toArray(), label))
				return cw;

			if(cw.hasSubCommand(label)){
				cw.getSubCommand(label);
			}
		}
		return null;
	}

	/**
	 * Inits the parent
	 *
	 * @param parent The parent
	 * @return This
	 */
	public CommandWrapper initParent(CommandWrapper parent){
		this.parent = parent;
		return this;
	}

	/**
	 * Inits the subCommand
	 *
	 * @param subCommands The subCommands
	 * @return This
	 */
	public CommandWrapper initChildrens(List<CommandWrapper> subCommands){
		this.subCommands = subCommands;
		return this;
	}

	// -- Intern methods

	public boolean hasSubCommand(String label){
		return getSubCommand(label) != null;
	}

	public boolean hasSubCommands(){
		return getSubCommands() != null && !getSubCommands().isEmpty();
	}

	public boolean hasParent(){
		return getParent() != null;
	}


}
