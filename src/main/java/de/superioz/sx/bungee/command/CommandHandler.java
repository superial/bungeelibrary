package de.superioz.sx.bungee.command;

import de.superioz.sx.bungee.BungeeLibrary;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.bungee.exception.CommandRegisterException;
import de.superioz.sx.java.util.Consumer;
import de.superioz.sx.java.util.ListUtil;
import net.md_5.bungee.api.CommandSender;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public class CommandHandler {

	private static List<CommandWrapper> commands = new ArrayList<>();
	public static final String EXECUTE_METHOD_NAME = "execute";

	/**
	 * Registers given command class
	 *
	 * @param commandClass The class
	 * @param subClasses   All classes of all sub commands
	 * @return The registering result
	 * @throws CommandRegisterException
	 */
	public static boolean registerCommand(final Consumer<CommandErrorEvent> onError, final Class<?> commandClass, Class<?>... subClasses) throws
			CommandRegisterException{
		if(!isCommandClass(commandClass)){
			throw new CommandRegisterException(CommandRegisterException.Reason.CLASS_NOT_COMMAND_CASE,
					commandClass);
		}

		// Get all commands
		List<Class<?>> classes = new ArrayList<>();
		classes.add(commandClass);
		classes.addAll(Arrays.asList(subClasses));
		List<CommandWrapper> fullCommands = getMethodalCommands(classes);

		// Get parent command
		// And initialises the parent/childrens
		CommandWrapper parentCommand = getParentCommand(commandClass);
		parentCommand = initRelations(parentCommand, fullCommands);

		for(CommandWrapper c : commands){
			if(c.equals(parentCommand)
					&& c.getLabel().equalsIgnoreCase(parentCommand.getLabel())){
				throw new CommandRegisterException(CommandRegisterException.Reason.COMMAND_ALREADY_EXISTS,
						commandClass);
			}
		}

		final CommandWrapper finalParentCommand = parentCommand;
		BungeeLibrary.proxy().getPluginManager().registerCommand(BungeeLibrary.plugin(),
				new net.md_5.bungee.api.plugin.Command(finalParentCommand.getLabel(),
						finalParentCommand.getPermission(), finalParentCommand.getAliases().toArray(new String[finalParentCommand.getAliases().size()])) {
					@Override
					public void execute(CommandSender commandSender, String[] strings){
						CommandExecutor executor = new CommandExecutor(finalParentCommand, commandClass, onError);
						executor.onCommand(commandSender, finalParentCommand.getLabel(), strings);
					}
				});
		if(!commands.contains(parentCommand)){
			commands.add(parentCommand);
		}

		return true;
	}

	/**
	 * Get all commands from methods from a specific class
	 *
	 * @param clazzes The classes
	 * @return The list of commands
	 */
	private static List<CommandWrapper> getMethodalCommands(List<Class<?>> clazzes){
		List<CommandWrapper> commmands = new ArrayList<>();

		for(Class<?> c : clazzes){
			for(Method m : c.getMethods()){
				if(isCommandMethod(m)){
					CommandWrapper cmd = new CommandWrapper(m, getCommandType(m));

					if(getCommand(cmd.getLabel(), commmands) == null){
						commmands.add(cmd);
					}
				}
			}
		}

		return commmands;
	}

	/**
	 * Initialises the parent and childrens from given commands
	 *
	 * @param parentCommand The parent
	 * @param oldCommands   All commands
	 * @return The list of commands with parent and childrens
	 */
	private static CommandWrapper initRelations(CommandWrapper parentCommand, List<CommandWrapper> oldCommands){
		List<CommandWrapper> newCommands = new ArrayList<>();

		// Init parent
		parentCommand.initParent(parentCommand);
		for(CommandWrapper c : oldCommands){
			if(c.getCommandType() == CommandType.SUB){
				c.initParent(parentCommand);
			}
			else if(c.getCommandType() == CommandType.NESTED){
				Command.Nested nested = c.getParentMethod().getAnnotation(Command.Nested.class);
				String label = nested.parent();
				CommandWrapper parent = getCommand(label, oldCommands);

				if(parent == null){
					continue;
				}

				// Init parent
				c.initParent(parent);
			}

			// Add to new commands
			newCommands.add(c);
		}

		// Init children
		for(CommandWrapper c : newCommands){
			List<CommandWrapper> childrens = getChildrens(c, newCommands);
			c.initChildrens(childrens);
		}
		parentCommand.initChildrens(getChildrens(parentCommand, newCommands));

		return parentCommand;
	}

	/**
	 * Return the command from list with given label
	 *
	 * @param label    The label
	 * @param commands The commands
	 * @return The command
	 */
	private static CommandWrapper getCommand(String label, List<CommandWrapper> commands){
		for(CommandWrapper cw : commands){
			if(cw.getLabel().equalsIgnoreCase(label)){
				return cw;
			}
		}
		return null;
	}

	/**
	 * Get the children from given wrapper
	 *
	 * @param wrapper  The wrapper
	 * @param commands The command list
	 * @return The list
	 */
	private static List<CommandWrapper> getChildrens(CommandWrapper wrapper, List<CommandWrapper> commands){
		List<CommandWrapper> wrappers = new ArrayList<>();

		for(CommandWrapper c : commands){
			if(c.getParent() == null)
				continue;
			if(c.getParent().getLabel().equalsIgnoreCase(wrapper.getLabel())){
				wrappers.add(c);
			}
		}

		return wrappers;
	}

	/**
	 * Gets the parent class of given class
	 *
	 * @param c The class
	 * @return The command
	 * @throws CommandRegisterException
	 */
	private static CommandWrapper getParentCommand(Class<?> c) throws CommandRegisterException{
		return new CommandWrapper(getExecuteMethod(c), CommandType.ROOT);
	}

	/**
	 * Get all commands exists in registerlist
	 *
	 * @return The list of commands
	 */
	public static List<CommandWrapper> getAllCommands(){
		List<CommandWrapper> allCommands = new ArrayList<>();
		List<CommandWrapper> current = new ArrayList<>();
		List<CommandWrapper> switchCurrent = new ArrayList<>();

		current.addAll(getCommands());
		// Loop through all commands
		while(!current.isEmpty()){
			for(CommandWrapper c : current){
				if(!c.hasSubCommands())
					continue;
				switchCurrent.addAll(c.getSubCommands());
			}

			// Add all commands to list
			// And add commands for new loop
			allCommands.addAll(current);
			current.clear();
			current.addAll(switchCurrent);
			switchCurrent.clear();
		}

		return allCommands;
	}

	/**
	 * Get commands of given label
	 *
	 * @param label The label
	 * @return The list of commands
	 */
	public static List<CommandWrapper> getCommands(String label){
		List<CommandWrapper> l = new ArrayList<>();
		for(CommandWrapper wrapper : getCommands()){
			if(wrapper.getLabel().equalsIgnoreCase(label)){
				l.add(wrapper);
			}
		}
		return l;
	}

	/**
	 * Get the command wrapper of given label
	 *
	 * @param label The label as string
	 * @return The command
	 */
	public static CommandWrapper getCommand(String label){
		for(CommandWrapper wr : getCommands(label)){
			if(wr.getCommandType() != CommandType.SUB){
				return wr;
			}
		}
		return null;
	}

	/**
	 * Get method for executing the command
	 *
	 * @param c The class
	 * @return The method
	 * @throws CommandRegisterException
	 */
	private static Method getExecuteMethod(Class<?> c) throws CommandRegisterException{
		try{
			return c.getDeclaredMethod(EXECUTE_METHOD_NAME, CommandContext.class);
		}
		catch(NoSuchMethodException e){
			throw new CommandRegisterException(CommandRegisterException.Reason
					.CLASS_NOT_COMMAND_CASE, c);
		}
	}

	/**
	 * Returns the specific command mobType for given command method
	 *
	 * @param m The method
	 * @return The result as mobType
	 */
	private static CommandType getCommandType(Method m){
		if(m.isAnnotationPresent(Command.class)){
			if(m.isAnnotationPresent(Command.Nested.class))
				return CommandType.NESTED;
			return CommandType.SUB;
		}
		return CommandType.UNKNOWN;
	}

	// -- Intern methods

	public static List<CommandWrapper> getCommands(){
		return commands;
	}

	private static boolean isCommandClass(Class<?> c){
		return c.isAnnotationPresent(Command.class)
				&& ListUtil.listContains(c.getInterfaces(), CommandCase.class);
	}

	private static boolean isCommandMethod(Method m){
		return m.isAnnotationPresent(Command.class);
	}

	private static boolean isNestedCommandMethod(Method m){
		return m.isAnnotationPresent(Command.Nested.class) && isCommandMethod(m);
	}

}
