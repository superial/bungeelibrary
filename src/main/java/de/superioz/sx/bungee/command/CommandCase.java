package de.superioz.sx.bungee.command;

import de.superioz.sx.bungee.command.context.CommandContext;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
public interface CommandCase {

    /**
     * Executes the command
     *
     * @param context The context
     */
    void execute(CommandContext context);

}
