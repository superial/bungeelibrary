package de.superioz.sx.bungee.command.verification;

import de.superioz.sx.bungee.command.AllowedCommandSender;
import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.SimpleStringUtils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 10.04.2016.
 */
@Command(label = MethodVerifier.COMMAND_NAME,
		flags = {"a"},
		commandTarget = AllowedCommandSender.PLAYER)
public class MethodVerifierCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		ProxiedPlayer player = context.getSenderAsPlayer();
		if(context.getArgumentsLength() < 1){
			return;
		}
		String arg = context.getArgument(1);

		// Check arg
		if(arg == null || arg.isEmpty()
				|| !SimpleStringUtils.isInteger(arg)){
			return;
		}
		int id = Integer.valueOf(arg);

		// Check id
		if(!MethodVerifier.has(id)){
			return;
		}

		boolean result = context.hasFlag("a");
		MethodVerifier.verified(id, result, player);
	}

}
