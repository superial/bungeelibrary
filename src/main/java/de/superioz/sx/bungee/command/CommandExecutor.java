package de.superioz.sx.bungee.command;

import de.superioz.sx.bungee.command.context.CommandContext;
import de.superioz.sx.java.util.Consumer;
import de.superioz.sx.java.util.ListUtil;
import lombok.Getter;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * This class was created as a part of BukkitLibrary
 *
 * @author Superioz
 */
@Getter
public class CommandExecutor {

    protected CommandWrapper commandWrapper;
    protected CommandWrapper command;
    protected Class executorClass;
	protected Consumer<CommandErrorEvent> eventConsumer;

    public CommandExecutor(CommandWrapper commandWrapper, Class clazz, Consumer<CommandErrorEvent> eventConsumer){
        this.commandWrapper = commandWrapper;
        this.executorClass = clazz;
	    this.eventConsumer = eventConsumer;
    }

    /**
     * The on command method
     *
     * @param commandSender The sender
     * @param label         The command label
     * @param args          The args
     *
     * @return The result
     */
    public boolean onCommand(CommandSender commandSender, String label, String[] args){
        // Get command
        command = CommandHandler.getCommand(label);
        CommandContext context = new CommandContext(commandSender, command, args);

        // Check label
        if(label.equalsIgnoreCase(command.getLabel())
                || ListUtil.listContains(command.getAliases().toArray(), command.getLabel())){

            if(checkCommandContext(context, command)){
                executeCommand(context);
                return true;
            }
        }
        return false;
    }

    /**
     * Execute the command with given context
     *
     * @param context The context
     *
     * @return The result
     */
    private boolean executeCommand(CommandContext context){
        try{
            int length = context.getArgumentsLength();
            CommandWrapper command = commandWrapper;

            if(length >= 1){
                for(int i = 0; i < length; i++){
                    String arg = context.getArgument(i);

                    if(!command.hasSubCommand(arg)){
                        break;
                    }
                    command = command.getSubCommand(arg);
                    context.setCommand(command);

                    // Check command
                    if(!checkCommandContext(context, command)){
                        return false;
                    }
                }
            }

            Method m = (command.getCommandType() == CommandType.SUB
                    || command.getCommandType() == CommandType.NESTED)
                    ? command.getParentMethod() : getExecuteCommand(getCommandWrapper().getParentClass());
            assert m != null;

            // Check command
            if(!checkCommandContext(context, command)){
                return false;
            }

            if(Modifier.isStatic(m.getModifiers())){
                m.invoke(null, context);
            }
            else{
                m.invoke(m.getDeclaringClass().newInstance(), context);
            }

            return true;
        }catch(IllegalAccessException
                | IllegalArgumentException | InvocationTargetException
                | InstantiationException e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get the execute method for given class
     *
     * @param clazz The class
     *
     * @return The method
     */
    private Method getExecuteCommand(Class<?> clazz){
        try{
            return clazz.getDeclaredMethod(CommandHandler.EXECUTE_METHOD_NAME, CommandContext.class);
        }catch(NoSuchMethodException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Check given context for given command
     *
     * @param context The context
     * @param command The command
     *
     * @return The result
     */
    private boolean checkCommandContext(CommandContext context, CommandWrapper command){
        CommandSender commandSender = context.getSender();

        // Check commandSender
        if(commandSender instanceof ProxiedPlayer){

            // Does the context allows a player
            if(!context.allows(AllowedCommandSender.PLAYER)){
                eventConsumer.accept(new CommandErrorEvent(commandSender, context, ErrorReason.WRONG_SENDER));
                return false;
            }

            // Player
	        ProxiedPlayer player = (ProxiedPlayer) context.getSender();
            String permission = command.getPermission();

            if(permission != null
                    && !permission.isEmpty()){
                if(!player.hasPermission(permission)){
                    eventConsumer.accept(new CommandErrorEvent(commandSender, context, ErrorReason.NO_PERMISSIONS));
                    return false;
                }
            }
        }
        else{

            // Does the context allows the console
            if(!context.allows(AllowedCommandSender.CONSOLE)){
	            // Failed
                return false;
            }
        }

        // Check arguments length
        if((context.getArgumentsLength() > command.getMax()
                && !(command.getMax() <= 0))
                || context.getArgumentsLength() < command.getMin()){

            // Fire event
            eventConsumer.accept(new CommandErrorEvent(commandSender, context, ErrorReason.WRONG_USAGE));
            return false;
        }
        return true;
    }

	public enum ErrorReason {

		NO_PERMISSIONS,
		WRONG_SENDER,
		WRONG_USAGE

	}

}
