package de.superioz.sx.bungee.command;

import de.superioz.sx.bungee.command.context.CommandContext;
import lombok.Getter;
import net.md_5.bungee.api.CommandSender;

/**
 * Created on 08.04.2016.
 */
@Getter
public class CommandErrorEvent {

	private CommandSender sender;
	private CommandContext context;
	private CommandExecutor.ErrorReason reason;

	public CommandErrorEvent(CommandSender sender, CommandContext context, CommandExecutor.ErrorReason reason){
		this.sender = sender;
		this.context = context;
		this.reason = reason;
	}

}
