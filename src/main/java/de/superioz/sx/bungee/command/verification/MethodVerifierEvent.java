package de.superioz.sx.bungee.command.verification;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created on 10.04.2016.
 */
@Getter
public class MethodVerifierEvent {

	private boolean result;
	private ProxiedPlayer player;

	public MethodVerifierEvent(boolean result, ProxiedPlayer player){
		this.result = result;
		this.player = player;
	}

}
