package de.superioz.sx.bungee.test;

import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.context.CommandContext;

/**
 * Created on 01.05.2016.
 */
public class TestCommand2 {

	@Command(label = "sub")
	public void on(CommandContext context){
		System.out.println("SUBCOMMAND!");
	}

}
