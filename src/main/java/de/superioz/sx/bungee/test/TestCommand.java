package de.superioz.sx.bungee.test;

import de.superioz.sx.bungee.command.Command;
import de.superioz.sx.bungee.command.CommandCase;
import de.superioz.sx.bungee.command.context.CommandContext;

/**
 * Created on 01.05.2016.
 */
@Command(label = "test")
public class TestCommand implements CommandCase {

	@Override
	public void execute(CommandContext context){
		System.out.println("TEST!");
	}

}
